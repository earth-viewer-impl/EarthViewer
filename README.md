# EarthViewer
Basic Earth viewer used to display an Earth.

Hobbyist project to play around with tesselation shaders and cubemaps.

![imgur](https://i.imgur.com/a1dJOs7.png)
![imgur](https://i.imgur.com/JkPf4FU.png)
![imgur](https://i.imgur.com/vS3dnBa.png)
![imgur](https://i.imgur.com/K1ix2zx.png)
![imgur](https://i.imgur.com/93qGaf0.png)
